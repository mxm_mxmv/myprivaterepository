#include "Header.h"

bool isPrimeNum(int num)
{
	for (int i = 2; i <= sqrt(num); ++i)
	{
		if (num % i == 0) return false;
	}

	return true;
}

int searchIntervalBoundary(char* string, const char* tokenToFind)
{
	if (strstr(string, tokenToFind))
	{
		int result = 0;
		char numbers[] = "0123456789";
		char* ptr = strpbrk(string, numbers);
		if (ptr)
		{
			char val[20];
			char* endPtr = strrchr(string, '<');//ATOI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			strncpy_s(val, ptr, ((endPtr - ptr) / sizeof(char)));
			result = atoi(val);
		}
		return result;
	}
	else exit;//???????????!!!!!!!!!!(0) 
}

ostream & operator<<(ostream & os, const pairInterval & obj)
{
	os << "low: " << obj.start << "   high: " << obj.finish << endl;

	return os;
}

void getPrimesFromPairImterval(set<int> & primesCollector, const pairInterval  _interval)
{
	for (int i = _interval.getStart(); i <= _interval.getFinish(); ++i)
	{
		if (isPrimeNum(i))
			primesCollector.insert(i);
	}
}