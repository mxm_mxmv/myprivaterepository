#pragma once
#include <iterator>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <thread>
#include <set>
#include <cstring>
#include <memory>

using namespace std;

//is the number prime or it is not
bool isPrimeNum(int num);

//searches for a beginning or an end of an interval
int searchIntervalBoundary(char* string, const char* tokenToFind);

//stores beginning and end of an interval
class pairInterval
{
	int start;
	int finish;
public:
	//pairInterval() : start(0), finish(0) {}
	pairInterval(int s, int f) : start(s), finish(f) {}
	//void setInterval(int s, int f) { start = s; finish = f; }
	void setStart(int s) { start = s; }
	void setFinish(int f) { finish = f; }
	int getStart() const { return start; }
	int getFinish() const { return finish; }

	pairInterval& operator=(const pairInterval& obj)
	{
		start = obj.start;
		finish = obj.finish;
	}
	friend ostream& operator<<(ostream& os, const pairInterval& obj);
};

//gets primes from an interval and stores them to a SET container
void getPrimesFromPairImterval(set<int> & primesCollector, const pairInterval  _interval);
