#include "Header.h"
//#include <memory>

using namespace std;

void main()
{
	ifstream ifs("XMLfile.txt");
	char buf[100];//an each-line-temp-buffer
	char _LOW_[] = "low";
	char _HIGH_[] = "high";
	vector<pairInterval> vecIntervalsCollector;

	bool isStartFound = false;
	bool isEndFound = false;
	int first = 0, second = 0, boundaryVal = 0;

	while (ifs.getline(buf, sizeof(buf)))
	{
		if (boundaryVal = searchIntervalBoundary(buf, _LOW_))
		{
			isStartFound = true;
			first = boundaryVal;
		}
		if (boundaryVal = searchIntervalBoundary(buf, _HIGH_))
		{
			isEndFound = true;
			second = boundaryVal;
		}

		if (isStartFound && isEndFound)
		{
			vecIntervalsCollector.emplace_back(first, second);
			isStartFound = isEndFound = false;
		}
	}

	//display intervals placed in vector
	for (auto i : vecIntervalsCollector)
		cout << i;

	//create a set of prime numbers
	set<int> setPrimesCollector;
	for (auto i : vecIntervalsCollector)
		getPrimesFromPairImterval(setPrimesCollector, i);

	//display the set of prime numbers
	for (auto i : setPrimesCollector)
		cout << i << " ";


	cout << endl;
	system("pause");
}

